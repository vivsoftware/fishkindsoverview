package com.gmail.ivanytskyy.vitaliy.fishkindsoverview.activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.gmail.ivanytskyy.vitaliy.fishkindsoverview.R;
import com.gmail.ivanytskyy.vitaliy.fishkindsoverview.model.Fish;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class MainActivity extends AppCompatActivity {
    private List<String> mAllFishKinds = new ArrayList<>();
    private List<Integer> mAllFishDrawableIdentifier = new ArrayList<>();
    private List<Integer> mAllFishDescriptionIdentifier = new ArrayList<>();
    private int mShortFishDescriptionLength;
    private final int REQUEST_CODE_SECOND_ACTIVITY = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mShortFishDescriptionLength = getResources().getInteger(R.integer.fish_short_description_length);
        String[] keyArray = {"image_key", "label_key", "description_key"};
        String[] kindArray = getResources().getStringArray(R.array.fish_kinds);
        initAllResourceLists(kindArray);
        ListView listView = (ListView) findViewById(R.id.listView);
        int[] itemComponentIdentifiers = {R.id.itemImageView, R.id.itemFishKindTextView, R.id.itemFishDescriptionTextView};
        SimpleAdapter adapter = new SimpleAdapter(this, initData(keyArray), R.layout.item_layout, keyArray, itemComponentIdentifiers);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                Fish fish = new Fish(
                        mAllFishKinds.get(position),
                        mAllFishDescriptionIdentifier.get(position),
                        mAllFishDrawableIdentifier.get(position));
                intent.putExtra(SecondActivity.FISH_EXTRA_KEY, fish);
                startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
            }
        });
    }
    private void initAllResourceLists(String[] fishKinds){
        mAllFishKinds = new ArrayList<>();
        mAllFishDrawableIdentifier = new ArrayList<>();
        mAllFishDescriptionIdentifier = new ArrayList<>();
        for(String fishKind : fishKinds){
            int drawableResourceIdentifier = getResources().getIdentifier(
                    fishKind.toLowerCase(),
                    "drawable",
                    getApplicationContext().getPackageName());
            int descriptionResourceIdentifier = getResources().getIdentifier(
                    fishKind.toLowerCase(),
                    "string",
                    getApplicationContext().getPackageName());
            if(drawableResourceIdentifier != 0 && descriptionResourceIdentifier != 0){
                mAllFishKinds.add(fishKind);
                mAllFishDrawableIdentifier.add(drawableResourceIdentifier);
                mAllFishDescriptionIdentifier.add(descriptionResourceIdentifier);
            }
        }
    }
    private List<Map<String, Object>> initData(String[] keyArray){
        List<Map<String, Object>> data = new ArrayList<>();
        Map<String, Object> stringObjectMap;
        for (int i = 0; i < mAllFishKinds.size(); i++){
            stringObjectMap = new HashMap<>();
            String description = getResources()
                    .getString(mAllFishDescriptionIdentifier.get(i))
                    .substring(0, mShortFishDescriptionLength) + " ...";
            stringObjectMap.put(keyArray[0], mAllFishDrawableIdentifier.get(i));
            stringObjectMap.put(keyArray[1], mAllFishKinds.get(i));
            stringObjectMap.put(keyArray[2], description);
            data.add(stringObjectMap);
        }
        return data;
    }
}