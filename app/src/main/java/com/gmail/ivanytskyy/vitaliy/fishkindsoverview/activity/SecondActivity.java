package com.gmail.ivanytskyy.vitaliy.fishkindsoverview.activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.gmail.ivanytskyy.vitaliy.fishkindsoverview.R;
import com.gmail.ivanytskyy.vitaliy.fishkindsoverview.model.Fish;
public class SecondActivity extends AppCompatActivity {
    public static final String FISH_EXTRA_KEY = "com.gmail.ivanytskyy.vitaliy.fishkindsoverview.activity.secondactivity.fish";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ImageView imageView = (ImageView) findViewById(R.id.layoutImageView);
        TextView fishKindTextView = (TextView) findViewById(R.id.layoutFishKindTextView);
        TextView fishDescriptionTextView = (TextView) findViewById(R.id.layoutFishDescriptionTextView);
        Button backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        Intent intent = getIntent();
        if(intent != null){
            Fish fish = intent.getParcelableExtra(FISH_EXTRA_KEY);
            imageView.setImageResource(fish.getImageResource());
            fishKindTextView.setText(fish.getKind());
            fishDescriptionTextView.setText(fish.getDescriptionResource());
        }
    }
}
