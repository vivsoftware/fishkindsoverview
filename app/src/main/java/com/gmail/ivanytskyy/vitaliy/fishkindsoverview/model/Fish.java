package com.gmail.ivanytskyy.vitaliy.fishkindsoverview.model;
import android.os.Parcel;
import android.os.Parcelable;
/**
 * Created by Vitaliy Ivanytskyy on 22.03.2016.
 */
public class Fish implements Parcelable{
    private String mKind;
    private int mDescriptionResource;
    private int mImageResource;
    public Fish(String kind, int descriptionResource, int imageResource) {
        mKind = kind;
        mDescriptionResource = descriptionResource;
        mImageResource = imageResource;
    }
    protected Fish(Parcel in) {
        mKind = in.readString();
        mDescriptionResource = in.readInt();
        mImageResource = in.readInt();
    }
    public static final Creator<Fish> CREATOR = new Creator<Fish>() {
        @Override
        public Fish createFromParcel(Parcel in) {
            return new Fish(in);
        }

        @Override
        public Fish[] newArray(int size) {
            return new Fish[size];
        }
    };
    public String getKind() {
        return mKind;
    }
    public void setKind(String kind) {
        mKind = kind;
    }
    public int getDescriptionResource() {
        return mDescriptionResource;
    }
    public void setDescriptionResource(int descriptionResource) {
        mDescriptionResource = descriptionResource;
    }
    public int getImageResource() {
        return mImageResource;
    }
    public void setImageResource(int imageResource) {
        mImageResource = imageResource;
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKind);
        dest.writeInt(mDescriptionResource);
        dest.writeInt(mImageResource);
    }
}